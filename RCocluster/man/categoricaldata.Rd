\docType{data}
\name{categoricaldata}
\alias{categoricaldata}
\title{Simulated categorical Data-set}
\format{A data matrix with 1000 rows and 100 columns.}
\description{
  It is a categorical data-set simulated using Categorical distribution with 5 modalities. It consist of three clusters in rows and two
  clusters in columns. 
}
\examples{
data(categoricaldata)
}
\keyword{datasets}

