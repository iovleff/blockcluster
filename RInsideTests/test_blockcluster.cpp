// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; tab-width: 8; -*-
//
// Simple example with data in C++ that is passed to R, processed and a result is extracted
//
// Copyright (C) 2009         Dirk Eddelbuettel 
// Copyright (C) 2010 - 2011  Dirk Eddelbuettel and Romain Francois
//
// GPL'ed 

#include <RInside.h>                    // for the embedded R via RInside

int main(int argc, char *argv[])
{
    RInside R(argc, argv);              // create an embedded R instance
    R.parseEvalQ("library(\"blockcluster\")");
    R.parseEvalQ("data(\"binarydata\")");
    R.parseEvalQ("out<-coclusterBinary(binarydata,nbcocluster=c(2,3))");
    R.parseEvalQ("summary(out)");
    R.parseEvalQ("data(\"categoricaldata\")");
    R.parseEvalQ("out<-coclusterCategorical(categoricaldata,nbcocluster=c(2,3))");
    R.parseEvalQ("summary(out)");
    R.parseEvalQ("data(\"gaussiandata\")");
    R.parseEvalQ("out<-coclusterContinuous(gaussiandata,nbcocluster=c(2,3))");
    R.parseEvalQ("summary(out)");
    R.parseEvalQ("data(\"contingencydataunknown\")");
    R.parseEvalQ("out<-coclusterContingency(contingencydataunknown,nbcocluster=c(2,3))");
    R.parseEvalQ("summary(out)");
//    R.parseEvalQ("print(out)");
    exit(0);
}


