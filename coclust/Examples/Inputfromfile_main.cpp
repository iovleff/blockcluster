/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2011  Parmeet Singh Bhatia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
*/

/*
 * Project:  cocluster
 * created on: Dec 23, 2011
 * Author: Parmeet Singh Bhatia
 *
 **/

/** @file CoClustermain.cpp
 *  @brief This file implements the main function that is called from command line to perform co-clustering. The main function
 *  will do following in sequence:
 *  -# It reads the path of the option file from command line argument.
 *  -# It calls the IP::ReadFromOptionFile function to read the options from the option file.
 *  -# Finally it instantiates appropriate classes, calls the setter functions of CoCluster class and finally CoCluster::run
 *  to perform co-clustering.
 **/

#include <iostream>
#include <string>
#include <exception>
#include "../src/include/CoClustinclude.h"

using namespace std;

int main(int argc,char** argv)
{

  char* InOptionFile = NULL;
  IStrategy * p_Strategy_ = NULL;
  ICoClustModel * p_Model_ = NULL;
  IInit * p_Init_ = NULL;
  IAlgo * p_Algo_ = NULL;
  IData * p_Data_ = NULL;
  BinaryData * p_Bdata = NULL;
  ContinuousData * p_Realdata = NULL;
  ContingencyData * p_Cdata = NULL;
  InputParameters Inparameters_(1);
  std::list<std::string>::iterator it;
  bool semisupervised =false;
  int c;

  while((c=getopt(argc,argv,"f:"))!=-1)
  {
    switch (c) {
      case 'f':
        InOptionFile = optarg;
        break;
      default:
        break;
    }
  }

  try {
    if(InOptionFile)
    {
      Inparameters_.ReadFromOptionFile(InOptionFile);

      //set semisupervised variable
      semisupervised = Inparameters_.GetStrategy().SemiSupervised;

      StrategyParameters Sparam_ = Inparameters_.GetStrategyparameters();

      // set Initialization method
      switch (Inparameters_.GetStrategy().Init_) {
        case e_CEMInit:
          p_Init_ = new CEMInit();
          break;
        case e_FuzzyCEMInit:
          p_Init_ = new FuzzyCEMInit();
          break;
        case e_RandomInit:
          p_Init_ = new RandomInit();
          break;
        default:
          p_Init_ = new CEMInit();
          break;
      }

      // Set p_Algo_ and corresponding strategy p_Strategy_
      switch (Inparameters_.GetStrategy().Algo_)
      {
        case BEM:
          p_Algo_ = new EMAlgo();
          p_Strategy_ = new XStrategyAlgo(Sparam_);
          break;
        case BCEM:
          p_Algo_ = new CEMAlgo();
          p_Strategy_ = new XStrategyAlgo(Sparam_);
          break;
        case BSEM:
          p_Algo_ = new SEMAlgo();
          p_Strategy_ = new XStrategyforSEMAlgo(Sparam_);
          break;
        default:
          p_Algo_ = new EMAlgo();
          p_Strategy_ = new XStrategyAlgo(Sparam_);
          break;
      }


      ModelParameters Mparam = Inparameters_.GetModelparameters();

      if (!semisupervised) {
        switch (Inparameters_.GetStrategy().DataType_) {
          case Binary:
            // Read Data
            p_Data_ = new BinaryData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Bdata = dynamic_cast<BinaryData*>(p_Data_);
            Mparam.nbrowdata_ = p_Bdata->dataij().rows();
            Mparam.nbcoldata_ = p_Bdata->dataij().cols();
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_epsilonkl:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),Mparam);
                break;
              case pik_rhol_epsilon:
                p_Model_ = new BinaryLBModelequalepsilon(p_Bdata->dataij(),Mparam);
                break;
              case pi_rho_epsilonkl:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),Mparam);
                break;
              case pi_rho_epsilon:
                p_Model_ = new BinaryLBModelequalepsilon(p_Bdata->dataij(),Mparam);
                break;
              default:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),Mparam);
                break;
            }
            break;
          case Continuous:
            // Read Data
            p_Data_ = new ContinuousData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Realdata = dynamic_cast<ContinuousData*>(p_Data_);
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_sigma2kl:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),Mparam);
                break;
              case pik_rhol_sigma2:
                p_Model_ = new ContinuousLBModelequalsigma(p_Realdata->dataij(),Mparam);
                break;
              case pi_rho_sigma2kl:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),Mparam);
                break;
              case pi_rho_sigma2:
                p_Model_ = new ContinuousLBModelequalsigma(p_Realdata->dataij(),Mparam);
                break;
              default:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),Mparam);
                break;
            }
            break;
          case Contingency:
            // Read Data
            p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Cdata = dynamic_cast<ContingencyData*>(p_Data_);
            // set contingency Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_unknown:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),Mparam);
                break;
              case pi_rho_unknown:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),Mparam);
                break;
              case pik_rhol_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_Model_ = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),p_Cdata->mui(),p_Cdata->nuj(),Mparam);
                break;
              case pi_rho_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_Model_ = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),p_Cdata->mui(),p_Cdata->nuj(),Mparam);
                break;
              default:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),Mparam);
                break;
            }
            break;
            case Categorical:
              // Read Data
              p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
              p_Data_->read(semisupervised);
              p_Cdata = dynamic_cast<ContingencyData*>(p_Data_);
              Mparam.nbrowdata_ = p_Cdata->dataij().rows();
              Mparam.nbcoldata_ = p_Cdata->dataij().cols();
              // set categorical Model
              p_Model_ = new CategoricalLBModel(p_Cdata->dataij(),Mparam);
              break;
          default:
            break;
        }

      } else {

        switch (Inparameters_.GetStrategy().DataType_) {
          case Binary:
            // Read Data
            p_Data_ = new BinaryData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Bdata = dynamic_cast<BinaryData*>(p_Data_);
            Mparam.nbrowdata_ = p_Bdata->dataij().rows();
            Mparam.nbcoldata_ = p_Bdata->dataij().cols();
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_epsilonkl:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pik_rhol_epsilon:
                p_Model_ = new BinaryLBModelequalepsilon(p_Bdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pi_rho_epsilonkl:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pi_rho_epsilon:
                p_Model_ = new BinaryLBModelequalepsilon(p_Bdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              default:
                p_Model_ = new BinaryLBModel(p_Bdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
            }
            break;
          case Continuous:
            // Read Data
            p_Data_ = new ContinuousData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Realdata = dynamic_cast<ContinuousData*>(p_Data_);
            Mparam.nbrowdata_ = p_Realdata->dataij().rows();
            Mparam.nbcoldata_ = p_Realdata->dataij().cols();
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_sigma2kl:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pik_rhol_sigma2:
                p_Model_ = new ContinuousLBModelequalsigma(p_Realdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pi_rho_sigma2kl:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pi_rho_sigma2:
                p_Model_ = new ContinuousLBModelequalsigma(p_Realdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              default:
                p_Model_ = new ContinuousLBModel(p_Realdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
            }
            break;
          case Contingency:
            // Read Data
            p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
            p_Data_->read(semisupervised);
            p_Cdata = dynamic_cast<ContingencyData*>(p_Data_);
            Mparam.nbrowdata_ = p_Cdata->dataij().rows();
            Mparam.nbcoldata_ = p_Cdata->dataij().cols();
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_unknown:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pi_rho_unknown:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
                break;
              case pik_rhol_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_Model_ = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),p_Cdata->mui(),p_Cdata->nuj(),Mparam);
                break;
              case pi_rho_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_Model_ = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),p_Cdata->mui(),p_Cdata->nuj(),Mparam);
                break;
              default:
                p_Model_ = new ContingencyLBModel(p_Cdata->dataij(),Mparam);
                break;
            }
            break;
               case Categorical:
                // Read Data
                p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
                p_Data_->read(semisupervised);
                p_Cdata = dynamic_cast<ContingencyData*>(p_Data_);
                Mparam.nbrowdata_ = p_Cdata->dataij().rows();
                Mparam.nbcoldata_ = p_Cdata->dataij().cols();
                // set categorical Model
                p_Model_ = new CategoricalLBModel(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);

                break;
          default:
            break;
        }

      }


      // Set cocluster with given Algorithm and Model
      CoCluster * p_CoCluster_ = new CoCluster();
      p_CoCluster_->SetStrategy(p_Strategy_);
      p_CoCluster_->SetAlgo(p_Algo_);
      p_CoCluster_->SetModel(p_Model_);
      p_CoCluster_->SetInit(p_Init_);
      //Run co-clustering
      //bool successful = false;
        if(p_CoCluster_->run())
        {
          //successful = true;
          std::cout<<"\nCo-clustering successfully terminated"<<std::endl;
          p_Model_->ConsoleOut();
        }
        else
          std::cout<<"\nCo-clustering failed."<<std::endl;

      //release memory
      delete p_Strategy_;
      delete p_Model_;
      delete p_Init_;
      delete p_Data_;
      delete p_CoCluster_;
    }
  } catch (exception & err){
    cout<< err.what();
    return -1;
  }
    return 0;
  }



