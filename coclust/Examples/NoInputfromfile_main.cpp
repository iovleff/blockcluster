/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2012  Parmeet Singh Bhatia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
*/

/*
 * Project:  cocluster
 * created on: Feb 12, 2012
 * Author: Parmeet Singh Bhatia
 *
 **/

/** @file CoClustermaindebug.cpp
 *  @brief
 **/
#include <iostream>
#include <string>
#include <time.h>
#include <exception>
#include <memory>

#include "../src/include/CoClustinclude.h"

using namespace std;

int main(int argc,char** argv)
{
  bool semisupervised = false;
  std::string filename = "/home/modal-laptop/Downloads/Parmeet/Nominale/categoricaldata.csv";
  std::shared_ptr<ContingencyData> p_Data_(new ContingencyData(filename));
  if(!p_Data_->read(semisupervised))
  exit(1);
  StrategyParameters Sparam_;
  ModelParameters Mparam_;
  std::shared_ptr<CoCluster> p_CoCluster_;
  std::shared_ptr<IStrategy> p_Strategy_;
  std::shared_ptr<ICoClustModel> p_Model_;
  std::shared_ptr<IInit> p_Init_;
  std::shared_ptr<IAlgo> p_Algo_;

  //set up cocluster
  p_CoCluster_.reset(new CoCluster());
  //Set Strategy Parameters
  Sparam_.nbtry_ = 2;
  Sparam_.nbxem_ = 5;
  Sparam_.nbiter_xem_ = 100;
  Sparam_.nbiter_XEM_ = 500;
  Sparam_.stop_criteria_ = ICoClustModel::ParameterStopCriteria;
//  Sparam_.Stop_Criteria = &ICoClustModel::ParameterStopCriteria;
  //Set Model Parameters
  Mparam_.eps_xem_ = .00001;
  Mparam_.eps_XEM_ = .00000001;
  Mparam_.nbinititerations_ = 10;
  Mparam_.initepsilon_ = .01;
  Mparam_.epsilon_int_ = .01;
  Mparam_.nbiterations_int_ = 5;
  Mparam_.nbrowclust_ = 3;
  Mparam_.nbcolclust_ = 2;
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = p_Data_->dataij().rows();
  Mparam_.nbcoldata_ = p_Data_->dataij().cols();

  p_Algo_.reset(new EMAlgo());
  p_Init_.reset(new RandomInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  if(!semisupervised)
  p_Model_.reset(new CategoricalLBModel(p_Data_->dataij(),Mparam_));
  else
   p_Model_.reset(new CategoricalLBModel(p_Data_->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam_));

  p_CoCluster_->SetAlgo(p_Algo_.get());
  p_CoCluster_->SetInit(p_Init_.get());
  p_CoCluster_->SetModel(p_Model_.get());
  p_CoCluster_->SetStrategy(p_Strategy_.get());

  if(p_CoCluster_->run())
    cout<<"Successful.";
  else
    cout<<"failed.";

  return 0;

}



