/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2012  Parmeet Singh Bhatia

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : bhatia.parmeet@gmail.com , parmeet.bhatia@inria.fr
 */

/*
 * Project:  coclust::
 * created on: Sep 10, 2012
 * Purpose:  .
 * Author:   modal-laptop, parmeet.bhatia@inria.fr
 *
 **/

/** @file CoClustermainOpenMP.cpp
 *  @brief In this file .
 **/

#include <iostream>
#include <string>
#include <stdexcept>
#include <time.h>
#include <omp.h>

#include "../src/include/CoClustinclude.h"

using namespace std;
#ifdef OPENMP
int main( int argc, char** argv)
{
  char* InOptionFile = NULL;
  IStrategy * p_Strategy_ = NULL;
  IAlgo * p_Algo_ = NULL;
  ICoClustModel * p_Model_ = NULL;
  ICoClustModel * p_FinalModel_ = NULL;
  CoCluster * p_CoCluster_ = NULL;
  IInit * p_Init_ = NULL;
  IData * p_Data_ = NULL;
  BinaryData * p_Bdata = NULL;
  ContinuousData * p_Realdata = NULL;
  ContingencyData * p_Cdata = NULL;
  InputParameters Inparameters_(1);
  std::list<std::string>::iterator it;
  bool semisupervised = false;
  float Lmax = -RealMax;
  int c, nbthreads = omp_get_num_procs();

  while ((c = getopt(argc, argv, "f:t:")) != -1)
  {
    switch (c)
    {
      case 'f':
        InOptionFile = optarg;
        break;
      case 't':
        nbthreads = atoi(optarg);
        break;
      default:
        break;
    }
  }


  try
  {
    if (InOptionFile)
    {
      Inparameters_.ReadFromOptionFile(InOptionFile);
      semisupervised = Inparameters_.GetStrategy().SemiSupervised;

      ModelParameters Mparam = Inparameters_.GetModelparameters();
      //get data
      switch (Inparameters_.GetStrategy().DataType_)
      {
        case Binary:
          // Read Data
          p_Data_ = new BinaryData(Inparameters_.GetDatafilename());
          p_Data_->read(semisupervised);
          p_Bdata = dynamic_cast<BinaryData*> (p_Data_);
          Mparam.nbrowdata_ = p_Bdata->dataij().rows();
          Mparam.nbcoldata_ = p_Bdata->dataij().cols();
          break;
        case Continuous:
          // Read Data
          p_Data_ = new ContinuousData(Inparameters_.GetDatafilename());
          p_Data_->read(semisupervised);
          p_Realdata = dynamic_cast<ContinuousData*> (p_Data_);
          Mparam.nbrowdata_ = p_Realdata->dataij().rows();
          Mparam.nbcoldata_ = p_Realdata->dataij().cols();
          break;
        case Contingency:
          // Read Data
          p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
          p_Data_->read(semisupervised);
          if (Inparameters_.GetStrategy().Model_ == pi_rho_known
              || Inparameters_.GetStrategy().Model_ == pik_rhol_known)
          {
            it = Inparameters_.GetOptionalfilenames().begin();
            p_Cdata->readmui(*(it));
            p_Cdata->readnuj(*(++it));
          }
          p_Cdata = dynamic_cast<ContingencyData*> (p_Data_);
          Mparam.nbrowdata_ = p_Cdata->dataij().rows();
          Mparam.nbcoldata_ = p_Cdata->dataij().cols();
          break;
        case Categorical:
          // Read Data
          p_Data_ = new ContingencyData(Inparameters_.GetDatafilename());
          p_Data_->read(semisupervised);
          p_Cdata = dynamic_cast<ContingencyData*> (p_Data_);
          Mparam.nbrowdata_ = p_Cdata->dataij().rows();
          Mparam.nbcoldata_ = p_Cdata->dataij().cols();
          break;
        default:
          throw std::runtime_error("Wrong Data-type");
          break;
      }

      if (!semisupervised)
      {
        switch (Inparameters_.GetStrategy().DataType_)
        {
          case Binary:
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_epsilonkl:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(), Mparam);
                break;
              case pik_rhol_epsilon:
                p_FinalModel_
                    = new BinaryLBModelequalepsilon(p_Bdata->dataij(), Mparam);
                break;
              case pi_rho_epsilonkl:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(), Mparam);
                break;
              case pi_rho_epsilon:
                p_FinalModel_
                    = new BinaryLBModelequalepsilon(p_Bdata->dataij(), Mparam);
                break;
              default:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(), Mparam);
                break;
            }
            break;
          case Continuous:
            // set continuous Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_sigma2kl:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      Mparam);
                break;
              case pik_rhol_sigma2:
                p_FinalModel_
                    = new ContinuousLBModelequalsigma(p_Realdata->dataij(),
                                                      Mparam);
                break;
              case pi_rho_sigma2kl:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      Mparam);
                break;
              case pi_rho_sigma2:
                p_FinalModel_
                    = new ContinuousLBModelequalsigma(p_Realdata->dataij(),
                                                      Mparam);
                break;
              default:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      Mparam);
                break;
            }
            break;
          case Contingency:

            // set contingency Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_unknown:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       Mparam);
                break;
              case pi_rho_unknown:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       Mparam);
                break;
              case pik_rhol_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_FinalModel_
                    = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),
                                                       p_Cdata->mui(),
                                                       p_Cdata->nuj(), Mparam);
                break;
              case pi_rho_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_FinalModel_
                    = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),
                                                       p_Cdata->mui(),
                                                       p_Cdata->nuj(), Mparam);
                break;
              default:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       Mparam);
                break;
            }
            break;
              case Categorical:
                // set categorical Model
                p_FinalModel_  = new CategoricalLBModel(p_Cdata->dataij(),Mparam);
                break;
          default:
            throw runtime_error("Wrong Data-type");
            break;
        }

      } else
      {
        switch (Inparameters_.GetStrategy().DataType_)
        {
          case Binary:
            // set Binary Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_epsilonkl:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(),
                                                  p_Data_->GetRowLabels(),
                                                  p_Data_->GetColLabels(),
                                                  Mparam);
                break;
              case pik_rhol_epsilon:
                p_FinalModel_
                    = new BinaryLBModelequalepsilon(p_Bdata->dataij(),
                                                    p_Data_->GetRowLabels(),
                                                    p_Data_->GetColLabels(),
                                                    Mparam);
                break;
              case pi_rho_epsilonkl:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(),
                                                  p_Data_->GetRowLabels(),
                                                  p_Data_->GetColLabels(),
                                                  Mparam);
                break;
              case pi_rho_epsilon:
                p_FinalModel_
                    = new BinaryLBModelequalepsilon(p_Bdata->dataij(),
                                                    p_Data_->GetRowLabels(),
                                                    p_Data_->GetColLabels(),
                                                    Mparam);
                break;
              default:
                p_FinalModel_ = new BinaryLBModel(p_Bdata->dataij(),
                                                  p_Data_->GetRowLabels(),
                                                  p_Data_->GetColLabels(),
                                                  Mparam);
                break;
            }
            break;
          case Continuous:
            // set continuous Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_sigma2kl:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      p_Data_->GetRowLabels(),
                                                      p_Data_->GetColLabels(),
                                                      Mparam);
                break;
              case pik_rhol_sigma2:
                p_FinalModel_
                    = new ContinuousLBModelequalsigma(p_Realdata->dataij(),
                                                      p_Data_->GetRowLabels(),
                                                      p_Data_->GetColLabels(),
                                                      Mparam);
                break;
              case pi_rho_sigma2kl:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      p_Data_->GetRowLabels(),
                                                      p_Data_->GetColLabels(),
                                                      Mparam);
                break;
              case pi_rho_sigma2:
                p_FinalModel_
                    = new ContinuousLBModelequalsigma(p_Realdata->dataij(),
                                                      p_Data_->GetRowLabels(),
                                                      p_Data_->GetColLabels(),
                                                      Mparam);
                break;
              default:
                p_FinalModel_ = new ContinuousLBModel(p_Realdata->dataij(),
                                                      p_Data_->GetRowLabels(),
                                                      p_Data_->GetColLabels(),
                                                      Mparam);
                break;
            }
            break;
          case Contingency:

            // set contingency Model
            switch (Inparameters_.GetStrategy().Model_)
            {
              case pik_rhol_unknown:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       p_Data_->GetRowLabels(),
                                                       p_Data_->GetColLabels(),
                                                       Mparam);
                break;
              case pi_rho_unknown:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       p_Data_->GetRowLabels(),
                                                       p_Data_->GetColLabels(),
                                                       Mparam);
                break;
              case pik_rhol_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_FinalModel_
                    = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),
                                                       p_Data_->GetRowLabels(),
                                                       p_Data_->GetColLabels(),
                                                       p_Cdata->mui(),
                                                       p_Cdata->nuj(),
                                                       Mparam);
                break;
              case pi_rho_known:
                it = Inparameters_.GetOptionalfilenames().begin();
                p_Cdata->readmui(*(it));
                p_Cdata->readnuj(*(++it));
                p_FinalModel_
                    = new ContingencyLBModel_mu_i_nu_j(p_Cdata->dataij(),
                                                       p_Data_->GetRowLabels(),
                                                       p_Data_->GetColLabels(),
                                                       p_Cdata->mui(),
                                                       p_Cdata->nuj(),
                                                       Mparam);
                break;
              default:
                p_FinalModel_ = new ContingencyLBModel(p_Cdata->dataij(),
                                                       p_Data_->GetRowLabels(),
                                                       p_Data_->GetColLabels(),
                                                       Mparam);
                break;
            }
            break;
              case Categorical:
               // set categorical Model
               p_FinalModel_ = new CategoricalLBModel(p_Cdata->dataij(),p_Data_->GetRowLabels(),p_Data_->GetColLabels(),Mparam);
               break;
          default:
            throw runtime_error("Wrong Data-type");
            break;
        }
      }

      StrategyParameters Sparam_ = Inparameters_.GetStrategyparameters();
      int nbtry = Sparam_.nbtry_;
      Sparam_.nbtry_ = 1;
      float start;

      //omp_set_dynamic(1);
      omp_set_num_threads(nbthreads);
      //start timing
      start = omp_get_wtime();
#pragma omp parallel shared(Lmax,Sparam_,nbtry) private(p_Model_,p_CoCluster_,p_Algo_,p_Init_,p_Strategy_)
      {
        // Set p_Algo_ and corresponding strategy p_Strategy_
        switch (Inparameters_.GetStrategy().Algo_)
        {
          case BEM:
            p_Algo_ = new EMAlgo();
            p_Strategy_ = new XStrategyAlgo(Sparam_);
            break;
          case BCEM:
            p_Algo_ = new CEMAlgo();
            p_Strategy_ = new XStrategyAlgo(Sparam_);
            break;
          case BSEM:
            p_Algo_ = new SEMAlgo();
            p_Strategy_ = new XStrategyforSEMAlgo(Sparam_);
            break;
          default:
            p_Algo_ = new EMAlgo();
            p_Strategy_ = new XStrategyAlgo(Sparam_);
            break;
        }

        // get copy of model for each thread
        p_Model_ = p_FinalModel_->Clone();

        // set Initialization method
        switch (Inparameters_.GetStrategy().Init_)
        {
          case e_CEMInit:
            p_Init_ = new CEMInit();
            break;
          case e_FuzzyCEMInit:
            p_Init_ = new FuzzyCEMInit();
            break;
          case e_RandomInit:
            p_Init_ = new RandomInit();
            break;
          default:
            p_Init_ = new CEMInit();
            break;
        }

        //set cocluster
        p_CoCluster_ = new CoCluster();
        p_CoCluster_->SetStrategy(p_Strategy_);
        p_CoCluster_->SetAlgo(p_Algo_);
        p_CoCluster_->SetModel(p_Model_);
        p_CoCluster_->SetInit(p_Init_);

        #pragma omp critical
         {
         cout<<omp_get_thread_num()<<" "<<sched_getcpu()<<'\n';
         }

#pragma omp for schedule(dynamic,1)

        for (int i = 0; i < nbtry; ++i)
        {
          p_CoCluster_->run();
#pragma omp critical
          {
            if (Lmax < p_Model_->GetLikelihood())
            {
              Lmax = p_Model_->GetLikelihood();
              delete p_FinalModel_;
              p_FinalModel_ = p_Model_->Clone();
            }
            cout << "Likelihood from thread num: " << omp_get_thread_num()
                << " is " << p_Model_->GetLikelihood() << "\n";
          }
        }

        delete p_Model_;
        delete p_Algo_;
        delete p_Init_;
        delete p_Strategy_;
        delete p_CoCluster_;

      }

      delete p_Data_;
      std::cout << "Time taken:" << omp_get_wtime() - start << "\n";
      p_FinalModel_->ConsoleOut();
      cout << "Likelihood: " << p_FinalModel_->GetLikelihood() << "\n";
      cout << "Lmax: " << Lmax << "\n";
      delete p_FinalModel_;
    }
  }
  catch (std::exception & err)
  {
    cout << err.what();
    return -1;
  }
  return 0;
}
#else
int main()
{
  std::cout<<"Open MP is not defined for this mode";
}
#endif
