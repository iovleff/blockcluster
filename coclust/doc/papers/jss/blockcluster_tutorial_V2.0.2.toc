\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Package details}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}cocluster function}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}cocluststrategy function}{3}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Understanding various input parameters}{5}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Example using simulated Binary dataset}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Examples with real datasets}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Image segmentation}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Document clustering}{8}{subsection.3.2}
\contentsline {section}{\numberline {4}Remarks}{8}{section.4}
