
\documentclass{beamer}
\mode<presentation>
{
  \usetheme{Warsaw}
  \setbeamercovered{transparent}
\setbeamertemplate{headline}{}
}
\input{preambule}
\usepackage[english]{babel}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{default}
\usepackage{multirow}
\title{Software for Co-Clustering of Binary data}
\date{May 22, 2012}
\author{Parmeet Bhatia $^{1}$ \& Serge Iovleff $^{2}$ \& Gérard Govaert $^{3}$}
\institute{$^{1}$  INRIA Lille

$^{2}$  Université  Lille 1 \& INRIA Lille

$^{3}$ Université de Technologie de Compiègne}
\begin{document}

\begin{frame}
 \maketitle
\vspace*{0.1cm}
\begin{center}
\begin{minipage}{0.1\textwidth}
\centering    \includegraphics[scale=0.1]{logos/inria.png}  
\end{minipage}
\hfill
\begin{minipage}{0.1\textwidth}
\centering \includegraphics[scale=0.08]{logos/utc.jpg}
\end{minipage}
\hfill
\begin{minipage}{0.1\textwidth}
\centering      \includegraphics[scale=0.12]{logos/ustl.png} 
\end{minipage}
 \end{center}
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Introduction to Co-Clustering}
\begin{frame}{Introduction}
\begin{block}{\bf{Co-Clustering}}

``Aims to organize data-set into a set of homogeneous blocks  by simultaneous clustering of individuals and variables.''
 
\end{block}
\pause
 \begin{figure}[h]
\centering
\includegraphics[width = 50mm]{binarysamplecocluster.png}
\caption{Binary data set (a),data reorganized by a partition on $I$ (b), by partitions on $I$ and $J$ simultaneously (c) and summary binary data (d).}
\label{fig:samplecoclust}
\end{figure}
\end{frame}

\section{Latent Block Models for Co-Clustering}
\begin{frame}{Mixture models}
\begin{block}{Classical Formulation}
\begin{eqnarray}
 f(\bsx_i;\btheta)=\sum_k \pi_k f_k(\bsx_i;\balpha), 
\end{eqnarray}
with $\bx = (\bsx_1,\ldots,\bsx_n)$ is i.i.d , $f_k(\bsx_i;\balpha)$ is $K$th density function.
\end{block}
\pause
\begin{block}{Density of the observed data}
\begin{eqnarray}
  f(\bx;\btheta)=\prod_i \sum_k  \pi_k f_k(\bsx_i;\balpha).
  \label{latent_block_model:eq:classical_mixture}
\end{eqnarray}
\end{block}
\end{frame}

\begin{frame}{New formulation for Mixture models}
\begin{block}{Density of the observed data}
  $$f(\bx;\btheta)=\prod_i \sum_k  \pi_k f_k(\bsx_i;\balpha).$$
\end{block}

 \begin{block}{New Formulation}
The pdf $f(\bx;\btheta)$ can be written as
\begin{eqnarray}
  f(\bx;\btheta)=\sum_{\bz \in \mathcal{Z}}  p(\bz) f(\bx|\bz;\balpha) 
  \label{latent_block_model:eq:classical_mixture_1}
\end{eqnarray}
where $\calZ$ is the classification of observations into clusters, $f(\bx|\bz;\balpha)=\prod_i f_{z_i}(\bsx_i;\balpha)$ and $p(\bz)=\prod_i \pi_{z_i}$.

\end{block}
\end{frame}


\begin{frame}{Latent block model}
  \begin{block}{Block mixture model for co-clustering problem}
$$f(\bx;\btheta)=\sum_{\bu \in \calU} p(\bu) f(\bx|\bu;\balpha),$$
where $\calU$ denotes the set of all possible co-clusters given the number of row and column clusters.
\end{block}
\pause
\begin{block}{Assumption:  conditional independence of blocks (co-clusters)}
 \begin{equation}
  \label{latent_block_model:eq:latent_block_model}
  f(\bx;\btheta)=\sum_{(\bz,\bw) \in \cal{Z \times W}}p(\bz) p(\bw) 
  f(\bx|\bz,\bw;\balpha),
\end{equation}
where $\calZ$ and $\calW$ denote the sets of all possible labellings $\bz$ of $I$ and $\bw$ of
$J$. 
\end{block}

\end{frame}

\begin{frame}{Bernoulli Latent block model for Binary data}
\begin{block}{Probability density function}
\begin{equation}
  \label{latent_block_model:eq:latent_block_model_1}
  f(\bx;\btheta)=\sum_{(\bz,\bw) \in \calZ \times \calW} 
  \prod_{i,k} \pi_{k}^{z_{ik}}\prod_{j,\ell} \rho_{l}^{w_{j\ell}} \prod_{i,j,k,\ell}\phi(x_{ij};\balpha_{k\ell})^{z_{ik}w_{j\ell}}
\end{equation}
where $\balpha_{k\ell} \in(0,1)$ and $\phi(x,\alpha_{k\ell}) = (\alpha_{k\ell})^{x}(1-\alpha_{k\ell})^{1-x}$
\end{block}
\pause
\begin{block}{Re-parameterization of Bernoulli distribution}
$$\phi_{k\ell}(x_{ij};\balpha)=(\varepsilon_{k\ell})^{|x_{ij}-a_{k\ell}|}
(1-\varepsilon_{k\ell})^{1-|x_{ij}-a_{k\ell}|}$$
where
$\left\{
  \begin{array}{ll}
    a_{k\ell}=0, \; \varepsilon_{k\ell}=\alpha_{k\ell} &\mbox{ if } \alpha_{k\ell}<0.5\\
    a_{k\ell}=1, \; \varepsilon_{k\ell}=1-\alpha_{k\ell} &\mbox{ if } \alpha_{k\ell}>0.5.
  \end{array}
\right.$
 
\end{block}
\end{frame}

\begin{frame}{Algorithms}

\begin{block}{Block EM algorithm}
\begin{enumerate}
 \item Initialize $\bt^{(0)},\br^{(0)}$ and $\btheta^{(0)}=(\bpi^{(0)},\brho^{(0)},\balpha^{(0)}).$
 \item Compute $\bt^{(c+1)},\bpi^{(c+1)},\balpha^{(c+(1/2))}$ by using EM algorithm for the data matrix $u_{i\ell}=\sum_j r_{j\ell} x_{ij}$ and starting 
from $\bt^{(c)},\bpi^{(c)},\balpha^{(c)}.$
 \item Compute $\br^{(c+1)},\brho^{(c+1)},\balpha^{(c+1)}$ by using EM algorithm for the data matrix $v_{jk}=\sum_i t_{ik} x_{ij}$ and starting from
 $\br^{(c)},\brho^{(c)},\balpha^{(c+(1/2))}.$
 \item Iterate steps (2) and (3) until convergence.
\end{enumerate}
\end{block}
\pause
\begin{block}{Block CEM algorithm}
\begin{itemize}
 \item Variant of BEM algorithm.
\item Addition of Classification-step in each of the EM algorithms(step 2 \& 3) above.
\end{itemize}

\end{block}

\end{frame}

\begin{frame}{XEM Strategy}
\begin{enumerate}
 \item Do several runs of: "initialization followed by short run of algorithm (few iterations/high tolerance)"
\item Select the best result of step 1 and make long run of Algorithm(high iterations/low tolerance)
\item Repeat step 1 and 2 several times and select the best result.
\end{enumerate}

\end{frame}




\section{CoClust Software: About, Usage and Examples}
\begin{frame}{CoClust Software}
\begin{itemize}
 \item {\bf CoCLust:} A software package implemented in C++ for co-clustering of binary, contingency and continuous data-sets.
\item Estimate parameters of the latent block models,gives classification, provides co-cluster visualization.
\pause
\begin{block}{Command Line}
\$ coclustermain -f path/to/optionfile.txt\\
optionfile.txt contains various input options required to run the co-clustering algorithm.
 
\end{block}

\end{itemize}
\end{frame}

\begin{frame}[fragile]{Rcoclust: R Package For coclustering}
 \begin{itemize}
  \item R interface for C++ library.
\item Simple and Robust API.
\item Extend all the four basic functions "Plot","Summary","Show","Print"
 \end{itemize}
%\fontsize{7}{8.2}\selectfont
\pause
 \begin{block}{A Simple Example}
>data(gaussiandata)\\
>newstrategy<-cocluststrategy(nbxem=5,nbtry=2,algo="XCEMStrategy")\\
>out<-cocluster(gaussiandata,datatype="Continuous",model= "\verb|pi_rho_sigma2kl|",nbcocluster=c(2,3),strategy=newstrategy)
\end{block}
\end{frame}

\begin{frame}{Examples}

\begin{figure}[h!]
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{data1.jpg}\\
\label{data1}
\centering(a)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering \includegraphics[width = 20mm,height=40mm]{cluster1.jpg}\\
\label{cluster1}
\centering(b)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{data2.jpg}\\
\label{data2}
\centering(c)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{cluster2.jpg}\\
\label{cluster2}
\centering(d)
\end{minipage}\\
\caption{Simulated data-1(a), co-clusters of data-1(b), Simulated data-2(c), co-clusters on data-2(d)}
\label{fig:datacluster}
\end{figure}

\end{frame}

\begin{frame}{True and estimated Parameters}
\fontsize{7}{8.2}\selectfont
 \begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c||c|c|c|}
\multicolumn{3}{r}{Data1} & \multicolumn{3}{r}{Data2}\\
\hline
\multirow{2}{2cm}{True values}&\color{green}0, 0.3&\color{green}0, 0.1&\color{green}1, 0.2&\color{green}1, 0.1&\color{green}1, 0.1&\color{green}0, 0.1\\
\cline{2-7}
&\color{green}1, 0.3&\color{green}1, 0.1&\color{green}0, 0.2&\color{green}0, 0.2&\color{green}0, 0.2&\color{green}1, 0.2\\
\hline
\multirow{2}{2cm}{Estimated values}&\color{red}0, 0.3051&\color{red}0, 0.0955&\color{red}1, 0.2065&\color{red}1, 0.0939&\color{red}1, 0.1103&\color{red}0, 0.0739\\
\cline{2-7}
&\color{red}1, 0.3036&\color{red}1, 0.1049&\color{red}0, 0.1951&\color{red}0, 0.0974&\color{red}0, 0.2117&\color{red}1, 0.2060\\
\hline
\end{tabular}
\caption{Comparison of true and estimated centers and dispersions}
\label{tab:dispersiontable}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c||c|c|c|}
\multicolumn{4}{r}{Data1} & \multicolumn{3}{c}{Data2}\\
\hline
True $\brho$&\color{green}.3&\color{green}.3&\color{green}.4&\color{green}.6&\color{green}.2&\color{green}.1\\
\hline
Estimated $\brho$&\color{red}0.29&\color{red}0.34&\color{red}0.37&\color{red}0.69&\color{red}0.186242&\color{red}0.123758\\
\hline
\end{tabular}
\caption{Comparison of true and estimated column proportions}
\label{tab:colproptable}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c||c|c|}
\multicolumn{3}{r}{Data1} & \multicolumn{2}{c}{Data2}\\
\hline
True $\bpi$&\color{green}.4&\color{green}.6&\color{green}.6&\color{green}.4\\
\hline
Estimated $\bpi$&\color{red}0.365&\color{red}0.635&\color{red}0.645&\color{red}0.355\\
\hline
\end{tabular}
\caption{Comparison of true and estimated row proportions}
\label{tab:rowproptable}
\end{table}
\end{frame}

\begin{frame}{Conclusion and future works}
\begin{itemize}
 \item Introduced newly developed {\bf CoClust} software and Rcoclust Package.
\item Pictorial views of the original and co-clustered data are also provided for analysis of the results.
\item Currently working on additional graphics.
\item R Package will soon be available on CRAN. 
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\fontsize{14}{8.2}\selectfont
  \bf{Thank you for your attention!}\\
\vspace{.5cm}
\bf{Questions??}
\end{center}
\end{frame}


\end{document}
