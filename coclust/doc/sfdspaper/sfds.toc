\beamer@endinputifotherversion {3.10pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction to Co-Clustering}{3}{0}{1}
\beamer@sectionintoc {2}{Latent Block Models for Co-Clustering}{5}{0}{2}
\beamer@sectionintoc {3}{CoClust Software: About, Usage and Examples}{15}{0}{3}
