\documentclass[12pt]{article}
\input{preambule}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{hyperref}
%---------------------------------------------------------------------
%
%\SetUnicodeOption
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}


%
%
% Retirez le caractere "%" au debut de la ligne ci--dessous si votre
% editeur de texte utilise des caracteres accentues
% \usepackage[latin1]{inputenc}

%
% Retirez le caractere "%" au debut des lignes ci--dessous si vous
% utiisez les symboles et macros de l'AMS
% \usepackage{amsmath}
% \usepackage{amsfonts}
%
%
\setlength{\textwidth}{16cm}
\setlength{\textheight}{21cm}
\setlength{\hoffset}{-1.4cm}
%
%
\begin{document}

%--------------------------------------------------------------------------

\begin{center}
{\Large
	{\sc  Software for co-clustering of Binary data}
}
\bigskip

Parmeet Bhatia $^{1}$ \& Serge Iovleff $^{2}$ \& Gérard Govaert $^{3}$

\bigskip

{\it
$^{1}$  Inria Lille, parmeet.bhatia@inria.fr

$^{2}$  Université  Lille 1 \& Inria Lille, serge.iovleff@inria.fr

$^{3}$ Université de Technologie de Compiègne,  gerard.govaert@utc.fr

}
\end{center}
\bigskip

%--------------------------------------------------------------------------

{
\bf R\'esum\'e.} Les techniques de Co-clustering ont pour but de segmenter
simultan\'ement les individus et les variables d'un jeu de donn\'ee afin de
cr\'eer des blocs de donn\'ees et de variables homog\`enes. Dans cet article nous
d\'ecrivons le logiciel {\bf CoClust}  qui permet de co-classifier des donn\'ees
binaires. Deux algorithmes ont \'et\'e impl\'ement\'es : Le ``Block EM'' et le
``Block CEM''. Une br\`eve introduction \`a ces deux m\'ethodes est faite.
L'utilisation du logiciel est illustr\'ee \`a travers quelques exemples.
%es images des donn\'ees originelles et des donn\'ees co-segment\'ees sont aussi
\smallskip

{\bf Mots-cl\'es.} Donn\'ees Binaires, Co-clustering, Mod\`eles \`a bloc latent,
Distribution de Bernoulli \bigskip\bigskip

{\bf Abstract.}
Co-clustering aims to organize data-set into a set of homogeneous blocks called co-clusters by simultaneously clustering the individuals and variables.
This paper attempts to introduce {\bf CoClust} software used for co-clustering of binary data-sets. Two algorithms have been
implemented namely Block EM and Block CEM. A brief review of these methods is made in subsequent sections. The usage of the software is illustrated with
the help of few examples.Pictorial views of the original and co-clustered data are also provided for analysis of the results.
\smallskip

{\bf Keywords.} Binary data-sets, Co-clustering, Latent Block Model, Bernoulli Distribution

%--------------------------------------------------------------------------

\section{Introduction}

Cluster analysis is a well known tool in a variety of scientific areas including pattern recognition, information retrieval, and data mining where the
task is to group similar individuals (or variables) together based on certain heuristics. Recently other methods have been developed, known as block
clustering methods, which consider the two sets (objects and variables) simultaneously and attempt to organize the data into homogeneous blocks.
Here we are concerned only with binary data-sets.
 \begin{figure}[h]
\centering
\includegraphics[width = 100mm]{binarysamplecocluster.png}
\caption{Binary data set (a),data reorganized by a partition on $I$ (b), by partitions on $I$ and $J$ simultaneously (c) and summary binary data (d).}
\label{fig:samplecoclust}
\end{figure}
For illustration, consider Figure~\ref{fig:samplecoclust} where a binary data set defined on set of $n=10$ individuals $I = {A,B,C,D,E,F,G,H,I,J}$ and set of
$d=7$ binary variables $J = {1,2,3,4,5,6,7}$ is re-organized into a set of $3\times3$ clusters by permuting the rows and columns.

In practice, several softwares exist for co-clustering such as Bicat [2] , biclust [3], BiGGEsTS [4] and BiVisu [5] to name a few.
In this paper, we attempt to introduce our newly developed {\bf CoClust} software which allows to estimate the parameters of the
Co-Clustering models [1] for binary data-sets. The new C++ library  is unique from the point of view of generative models it implements (latent blocks),
and the used algorithms (BEM, BCEM). Apart from that, special attention has been give to design the library which allows it to handle very huge data sets in reasonable time. For all the software related information, updates and downloads, we refer our readers to \url{http://modal.lille.inria.fr/}.
The paper is organized as follows. We briefly give an overview of latent block models developed for co-clustering of binary data-sets
followed by a brief description on algorithms implemented in {\bf CoClust} software. Finally we illustrate the usage of software with some examples
and conclude our paper with a note on future works.

\section{Co-Clustering model for Binary data}

\subsection{Notations}
Let the data-set $\bx$ be defined on a set $I$ with $n$ elements (individuals) and a set $J$ with $d$ elements (variables). We represent a partition of $I$
into $g$ clusters by $\bz=(z_{11},\ldots,z_{ng})$ with $z_{ik}=1$ if $i$ belongs to cluster $k$ and $z_{ik}=0$ otherwise, $z_i=k$ if $z_{ik}=1$ and
we denote by $z_{.k}=\sum_i z_{ik}$ the cardinal of cluster $k$. Similarly, we represent a partition of $J$ into $m$ clusters by $\bw=(w_{11},\ldots,w_{dm})$
with $w_{j\ell}=1$ if $j$ belongs to cluster $\ell$ and $w_{j\ell}=0$ otherwise, $w_j=\ell$ if $w_{j\ell}=1$ and we denote $w_{.\ell}=\sum_j w_{j\ell}$
the cardinal of cluster $\ell$. Furthermore, the parameters of the underlying mixture model are denoted by $\balpha$ and the row and column proportions
are respectively denoted by $\bpi=(\pi_{1},\ldots,\pi_{g})$ and $\brho=(\rho_{1},\ldots,\rho_{m})$.

\subsection{Definition}

Assuming the co-clusters (blocks) to be conditionally independent, the parameters $\balpha$ of the underlying distribution of binary data-set is given
by matrix $\bp=(p_{k\ell})$ where $p_{k\ell} \in
]0,1[$ $\forall$ $k=1,\ldots,g$ and $\ell=1,\ldots,m$ and the probability distribution $f_{k\ell}(x_{ij};\bp)=f(x_{ij};p_{k\ell})$ is the Bernoulli
distribution $$f(x_{ij};p_{k\ell})=(p_{k\ell})^{x_{ij}}(1-p_{k\ell})^{1-x_{ij}}.$$
we re-parameterize the model density as follows:
$$f_{k\ell}(x_{ij};\balpha)=(\varepsilon_{kj})^{|x_{ij}-a_{k\ell}|}
(1-\varepsilon_{kj})^{1-|x_{ij}-a_{k\ell}|}$$
where
$\left\{
  \begin{array}{ll}
    a_{k\ell}=0, \; \varepsilon_{k\ell}=p_{k\ell} &\mbox{ if } p_{k\ell}<0.5\\
    a_{k\ell}=1, \; \varepsilon_{k\ell}=1-p_{k\ell} &\mbox{ if } p_{k\ell}>0.5.
  \end{array}
\right.$

Hence the parameters $p_{k\ell}$ of the Bernoulli mixture model are replaced by the following parameters:
\begin{itemize}
\item The binary value $a_{k\ell}$, which acts as the center of the block $k,\ell$ and which
  gives, for each block, the most frequent binary value,
\item The value $\varepsilon_{k\ell}$ belonging to the set $]0,1/2[$ that characterizes the dispersion of the block $k,\ell$
  and which is, for each block, the probability of having a different value of the center.
\end{itemize}


\subsection{Latent block Model}

In general, EM algorithm tries to maximize the data log-likelihood over the set of parameters of the model which governs the underlying distribution of data by
alternating between the E-step and M-step until some convergence criteria is met. In case of Bernoulli latent block model, the complete data log-likelihood
takes the form
$$L_c(\bz,\bw,\btheta)=\sum_{k}  z_{.k} \log \pi_k + \sum_{\ell}  w_{.\ell} \log \rho_\ell
+\underbrace{\sum_{i,j,k,\ell}  z_{ik}w_{j\ell}\log f(x_{ij};p_{k\ell})}_{L_{CR}(\bz,\bw,\bp)}$$
where $\btheta=(\bpi,\brho,\balpha)$ and $L_{CR}$ is called restrained complete data log-likelihood and takes the form
$$L_{CR}(\bz,\bw,\bp)=\sum_{k\ell}\big(y_{k\ell}\log{p_{k\ell}}
+(z_{.k} w_{.\ell}-y_{k\ell})\log{(1-p_{k\ell})} \big).$$
and $y_{k\ell}=\sum_{i,j} z_{ik}w_{j\ell} x_{ij}$.
We can rewrite the restrained complete data log-likelihood in terms of previously defined explicit parameters as
$$L_{CR}(\bz,\bw,\balpha)=\sum_{i,j,k,\ell}  z_{ik} w_{j\ell} \left( |x_{ij} - a_{k\ell}|
 \log\frac{\varepsilon_{k\ell}}{1-\varepsilon_{k\ell}} +   \log{(1-\varepsilon_{k\ell})}\right).$$

In case of latent block model, we try to maximize restrained complete data log-likelihood. The interesting quantities to estimate the model are
the conditional row and column class
probabilities denoted by $\bt=(t_{ik}) $ and $\br=(r_{jl})$ respectively (refer [1] for derivation). We specify here only the final expression:
$$t_{ik}=\log \pi_k + \sum_\ell |u_{i\ell} - r_{.\ell} a_{k\ell} |\log
\frac{\varepsilon_{k\ell}}{1-\varepsilon_{k\ell}} + \sum_\ell r_{.\ell} \log{(1-\varepsilon_{k\ell})}$$
and
$$r_{j\ell} = \log \rho_\ell + \sum_k |v_{jk} - t_{.k} a_{k\ell} |\log
\frac{\varepsilon_{k\ell}}{1-\varepsilon_{k\ell}} + \sum_k t_{.k}
\log{(1-\varepsilon_{k\ell})}$$
where  $u_{i\ell}=\sum_j r_{j\ell} x_{ij}$ and $v_{jk}=\sum_i t_{ik} x_{ij}$. It can be easily shown that the values of model parameters maximizing $L_{CR}$ is given by
$$\left\{
  \begin{array}{ll}
    a_{k\ell}=0, \; \varepsilon_{k\ell}=\frac{y_{k\ell}}{t_{.k} r_{.\ell}}&\mbox{if}
    \quad \frac{y_{k\ell}}{t_{.k} r_{.\ell}}<0.5\\
    a_{k\ell}=1, \; \varepsilon_{k\ell}=1-\frac{y_{k\ell}}{t_{.k} r_{.\ell}} &\mbox{otherwise}.
  \end{array}
\right.$$


\section{CoClust software}


{\bf CoCLust} is a software package implemented in C++ for co-clustering of Binary data-sets.
It allows to estimate the parameters of the latent block model and gives the classification of rows and columns into various clusters. It implements the algorithms BEM2 and BCEM presented in [1], which we recall hereafter.

\subsection{Block EM Algorithm}

We have the following algorithm which is denoted by BEM2\footnote{There is another version called BEM1 but this algorithm is worse both in performance and time
consumption compared to BEM2, hence we only implemented BEM2.}.

\begin{enumerate}
 \item Initialize $\bt^{(0)},\br^{(0)}$ and $\btheta^{(0)}=(\bpi^{(0)},\brho^{(0)},\balpha^{(0)}).$
 \item Compute $\bt^{(c+1)},\bpi^{(c+1)},\balpha^{(c+(1/2))}$ by using EM algorithm for the data matrix $u_{i\ell}$ and starting
from $\bt^{(c)},\bpi^{(c)},\balpha^{(c)}.$
 \item Compute $\br^{(c+1)},\brho^{(c+1)},\balpha^{(c+1)}$ by using EM algorithm for the data matrix $v_{jk}$ and starting from
 $\br^{(c)},\brho^{(c)},\balpha^{(c+(1/2))}.$
 \item Iterate steps (2) and (3) until convergence.
\end{enumerate}

\subsection{BCEM Algorithm}

This algorithm, is a variant of BEM2 algorithm described above. To perform this algorithm, it is sufficient to add a C-step in each of the EM algorithms,
which converts the $t_{ik}$'s and $r_{jl}$'s to a discrete classification before performing the M-step by assigning each individual and variable to the cluster having the highest posterior probabilty.

% he salient features of the software includes
% efficient and robust implementation of algorithms apart from it very simple command line interface. To run the program from terminal, the following
% self-explainatory command need to issued:
% {\bf >./coclustermain -f path/to/optionfile/optionfile.txt .}
% For convinience, a sample option file is provided with the software.
% \paragraph{@Option File:} This is nothing but a text file with various options that are needed to run the algorithms. It provides a convinient way
% for the user to specify various options, instead of writing them all in the command line which could very combersome when there are many options
% to write. The file comes with default values for all the options but the datafile name. For most cases, user only have to provide the path to data file
% and edit the number of row and columns clusters entry in option file. Details of all the options is out of scope of this paper, but we would like to
% refer our readers well in advace to the detailed documentation which shall be soon  released with our software.
% \paragraph{@Output:} The software provide exhaustive list of output parameters for bernoulli Mixture model and the classification matrix
%  $\bz$ and $\bw$ for individuals and variables respectively.

\section{Usage and Examples}

The software currently comes with command line interface, and the user only have to provide the option file as a command line argument. The option
file contains all the user defined options, and the description for the same is out of scope of this article. We refer our readers to
\url{http://modal.lille.inria.fr/} for
complete description and usage of the software. Hereafter, we only provide description of the output of the software and the various estimates
with help of few examples.
Figure~\ref{fig:datacluster}(a) and \ref{fig:datacluster}(c) show two simulated binary data-sets of dimensions $200\times100$ with true parameters value shown
 in Table~\ref{tab:dispersiontable},
\ref{tab:colproptable} and \ref{tab:rowproptable}. Both data-sets contains $2$ row clusters and $3$ column clusters. Figure~\ref{fig:datacluster}(b) and \ref{fig:datacluster}(d) graphically
depicts the co-clustered data with the help of {\bf CoClust} software. The estimated parameters are shown in the respective tables along with true
parameters. These results clearly shows the quality of results for co-clustering produced by latent block models. The pictorial view of the co-clustered data
produced by the software also helps in analyzing the results to great extend.

\begin{figure}[h!]
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{data1.jpg}\\
\label{data1}
\centering(a)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering \includegraphics[width = 20mm,height=40mm]{cluster1.jpg}\\
\label{cluster1}
\centering(b)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{data2.jpg}\\
\label{data2}
\centering(c)
\end{minipage}
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width = 20mm,height=40mm]{cluster2.jpg}\\
\label{cluster2}
\centering(d)
\end{minipage}\\
\caption{Simulated data-1(a), co-clusters of data-1(b), Simulated data-2(c), co-clusters on data-2(d)}
\label{fig:datacluster}
\end{figure}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c||c|c|c|}
\multicolumn{3}{r}{Data1} & \multicolumn{3}{r}{Data2}\\
\hline
\multirow{2}{2cm}{True values}&0, 0.3&0, 0.1&1, 0.2&1, 0.1&1, 0.1&0, 0.1\\
\cline{2-7}
&1, 0.3&1, 0.1&0, 0.2&0, 0.2&0, 0.2&1, 0.2\\
\hline
\multirow{2}{2cm}{Estimated values}&0, 0.3051&0, 0.0955&1, 0.2065&1, 0.0939&1, 0.1103&0, 0.0739\\
\cline{2-7}
&1, 0.3036&1, 0.1049&0, 0.1951&0, 0.0974&0, 0.2117&1, 0.2060\\
\hline
\end{tabular}
\caption{Comparison of true and estimated centers and dispersions respectively}
\label{tab:dispersiontable}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c||c|c|c|}
\multicolumn{4}{r}{Data1} & \multicolumn{3}{c}{Data2}\\
\hline
True $\brho$&.3&.3&.4&.6&.2&.1\\
\hline
Estimated $\brho$&0.29&0.34&0.37&0.69&0.186242&0.123758\\
\hline
\end{tabular}
\caption{Comparison of true and estimated column proportions}
\label{tab:colproptable}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c||c|c|}
\multicolumn{3}{r}{Data1} & \multicolumn{2}{c}{Data2}\\
\hline
True $\bpi$&.4&.6&.6&.4\\
\hline
Estimated $\bpi$&0.365&0.635&0.645&0.355\\
\hline
\end{tabular}
\caption{Comparison of true and estimated row proportions}
\label{tab:rowproptable}
\end{table}


\section{Conclusion and Future works}
In this paper we have given a short overview of latent block model for co-clustering of binary data-sets and introduced our newly developed {\bf CoClust} software that currently works with binary
data-sets only.
The work on {\bf CoClust} software is currently active and the
current and future works include implementation of latent block models for categorical and continuous data-sets and subsequent development of
complete R-package. A laboratory version of R-package for co-clustering of binary data-sets have already been developed and is under testing phase.

\section{Acknowledgement}
We are grateful to Christophe Biernacki for reviewing the text and giving his useful suggestions.
%Quelques rappels :
%%
%\begin{center}
%%
%\begin{tabular}{lr} \hline
%%
%Accent aigu :              &  \'e; \\
%Accent grave :             &  \`a;\\
%Accent circonflexe :       &  \^o mais \^{\i};\\
%Tr\'emas :                 &  \"o mais \"{\i};\\
%C\'edille :                &  \c{c}. \\ \hline
%\end{tabular}
%%
%\end{center}

%--------------------------------------------------------------------------

\section*{Bibliographie}

\noindent [1] Gérard Govaert and Mohamed Nadif (2007), {\it Block clustering with Bernoulli mixture models: Comparison of different approaches},
Computational Statistics \& Data Analysis.

\noindent [2] Barkow S., Bleuler S., Prelic A., Zimmermann P., and E. Zitzler (2006), {\it BicAT: a biclustering analysis toolbox}, Bioinformatics.

\noindent [3] Kaiser Sebastian and Leisch Friedrich (2008), {\it  A Toolbox for Bicluster Analysis in R},
Department of Statistics: Technical Reports, No.28

\noindent [4] Joana P. Gonçalves, Sara C. Madeira and Arlindo L. Oliveira (2009), {\it BiGGEsTS: integrated environment for biclustering analysis of time series gene expression data},
BMC Research Notes.

\noindent [5] K.O. Cheng, N.F. Law, and W.C. Siu and A. W.C. Liew, {\it Use of Parallel Coordinate Plots in Biclustering Analysis of Gene Expression Data},
submitted to BMC Bioinformatics.
% \noindent [2] Achin, M. et Quidont, C. (2000), {\it Th\'eorie des
% Catalogues}, Editions du Soleil, Montpellier.
%
% \noindent [3] Noteur, U. N. (2003), Sur l'int\'er\^et des
% r\'esum\'es, {\it Revue des Organisateurs de Congr\`es}, 34, 67--89.
%
%
% Computational Statistics & Data Analysis
%
% Volume 52, Issue 6, 20 February 2008, Pages 3233–3245
% Cover image
\end{document}

