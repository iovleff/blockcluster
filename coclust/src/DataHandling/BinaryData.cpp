/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2012  Serge Iovleff

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : Serge.Iovleff@stkpp.org
 */

/** @file BinaryData.cpp
 *  @brief Implements BinaryData class that read Binary data from  text files.
 **/

#include "BinaryData.h"
//#include "../../../stkpp/include/DManager.h"
#include "../InputParameters/InputParameters.h"

BinaryData::BinaryData( std::string const& file_name)
                      : IData(file_name)
{ }

BinaryData::~BinaryData()
{ }

/*bool BinaryData::read()
{
  STK::ReadWriteCsv rw(fileName_,false,_T(","));
  std::string value;
  if(rw.read())
  {
    IP::nbcoldata_ = rw.size();
    IP::nbrowdata_ = rw.lastVe();
    m_Dataij.resize(IP::nbrowdata_,IP::nbcoldata_);
    for ( int i = 0; i < IP::nbrowdata_; ++i) {
       for ( int j = 0; j < IP::nbcoldata_; ++j) {
         m_Dataij(i,j)= std::atoi(rw(j+1,i+1).c_str());
       }
    }
    return true;
  }
  else
    return false;
}*/

bool BinaryData::read(bool semisupervised)
{
  std::fstream file;
  file.open(fileName_.c_str());
  if (!file.is_open()) {
#ifdef COVERBOSE
    std::cout<<"Input file cannot be open for reading.";
#endif
    return false;
  }
  std::string csvLine,val;
  int rows = 0,temprows = 0,cols = 0,tempcols = 0;
  std::istringstream csvStream;
  while( std::getline(file, csvLine) ){
    temprows++;
    csvStream.str(csvLine);
  }

  while( std::getline(csvStream, val, ',') ){
    tempcols++;
  }

  rows = temprows;
  cols = tempcols;
  if (!semisupervised) {
    m_Dataij.resize(rows,cols);
    temprows = 0 , tempcols = 0;
    file.close();
    file.open(fileName_.c_str(),std::ios::in);
    while( std::getline(file, csvLine) ){
      std::istringstream csvStream(csvLine);
      while( std::getline(csvStream,val, ',') ){
        m_Dataij(temprows,tempcols) = std::atoi(val.c_str());
        tempcols++;
      }
      temprows++,tempcols = 0;
    }
  } else {

    MatrixReal tempdata(rows,cols);
    m_Dataij.resize(rows-1,cols-1);
    temprows = 0 , tempcols = 0;
    file.close();
    file.open(fileName_.c_str(),std::ios::in);
    while( std::getline(file, csvLine) ){
      std::istringstream csvStream(csvLine);
      while( std::getline(csvStream,val, ',') ){
        tempdata(temprows,tempcols) = std::atoi(val.c_str());
        tempcols++;
      }
      temprows++,tempcols = 0;
    }
    m_Dataij     = tempdata.sub(STK::Range(1,rows-1), STK::Range(1,cols-1));
    v_RowLabels_ = tempdata.col(STK::Range(1,rows-1), 0);
    v_ColLabels_ = tempdata.row(0, STK::Range(1,cols-1));
    m_Dataij.shift(0,0);
    v_RowLabels_.shift(0);
    v_ColLabels_.shift(0);

//    m_Dataij = tempdata.block(1,1,rows-1,cols-1).cast<bool>();
//    v_RowLabels_ = tempdata.block(1,0,rows-1,1);
//    v_ColLabels_ = (tempdata.block(0,1,1,cols-1)).transpose();
  }
  return true;
}
