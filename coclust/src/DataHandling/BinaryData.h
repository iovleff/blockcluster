/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2012  Serge Iovleff

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : Serge.Iovleff@stkpp.org
 */

/** @file BinaryData.h
 *  @brief Declares BinaryData class that read Binary data from  text files.
 **/

#ifndef BINARYDATA_H_
#define BINARYDATA_H_

/** @brief This class provides functions for binary data handling.
 *
 */
#include "IData.h"
#include "../typedefs/typedef.h"

class BinaryData: public IData
{
  public:
    BinaryData(std::string const& file_name);
    virtual ~BinaryData();
    virtual bool read(bool);
    MatrixBinary const& dataij() const { return m_Dataij;}

  protected:
    MatrixBinary m_Dataij;

};

#endif /* BINARYDATA_H_ */
