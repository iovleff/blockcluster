/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2013  Parmeet Singh Bhatia

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
 */


/** @file ContingencyData.cpp
 *  @brief Implements ContingencyData class that read Contingency data from  text files.
 **/

#include "ContingencyData.h"
//#include "../../../stkpp/include/DManager.h"

#include "../InputParameters/InputParameters.h"

ContingencyData::ContingencyData( std::string const& file_name)
                      : IData(file_name)
{ }

ContingencyData::~ContingencyData()
{ }

/*bool ContingencyData::read()
{
  STK::ReadWriteCsv rw(fileName_,false,_T(","));
  std::string value;
  if(rw.read())
  {
    IP::nbcoldata_ = rw.size();
    IP::nbrowdata_ = rw.lastVe();
    m_Dataij.resize(IP::nbrowdata_,IP::nbcoldata_);
    for ( int i = 0; i < IP::nbrowdata_; ++i) {
       for ( int j = 0; j < IP::nbcoldata_; ++j) {
         m_Dataij(i,j)= std::atoi(rw(j+1,i+1).c_str());
       }
    }
    return true;
  }
  else
    return false;
}

bool ContingencyData::readmui(std::string const& file_name)
{
  STK::ReadWriteCsv rw(file_name,false,_T(","));
  std::string value;
  if(rw.read())
  {
    if(rw.lastVe()!=IP::nbrowdata_)
    {
      return false;
    }
    v_Mui.resize(IP::nbrowdata_);
    for ( int i = 0; i < IP::nbrowdata_; ++i) {
       for ( int j = 0; j < 1; ++j) {
         v_Mui(i)= std::atoi(rw(j+1,i+1).c_str());
       }
    }
    return true;
  }
  else
    return false;
}


bool ContingencyData::readnuj(std::string const& file_name)
{
  STK::ReadWriteCsv rw(file_name,false,_T(","));
  std::string value;
  if(rw.read())
  {
    if(rw.lastVe()!=IP::nbcoldata_)
    {
      return false;
    }
    v_Nuj.resize(IP::nbcoldata_);
    for ( int i = 0; i < IP::nbcoldata_; ++i) {
       for ( int j = 0; j < 1; ++j) {
         v_Nuj(i)= std::atoi(rw(j+1,i+1).c_str());
       }
    }
    return true;
  }
  else
    return false;
}*/

bool ContingencyData::read(bool semisupervised)
{
  std::fstream file;
  file.open(fileName_.c_str());
  if (!file.is_open()) {
#ifdef COVERBOSE
    std::cout<<"Input file cannot be open for reading.";
#endif
    return false;
  }
  std::string csvLine,val;
  int rows = 0,temprows = 0,cols = 0,tempcols = 0;
  std::istringstream csvStream;
  while( std::getline(file, csvLine) ){
    temprows++;
    csvStream.str(csvLine);
  }

  while( std::getline(csvStream, val, ',') ){
    tempcols++;
  }

  rows = temprows;
  cols = tempcols;
  if (!semisupervised)
  {
    m_Dataij.resize(rows,cols);
    temprows = 0 , tempcols = 0;
    file.close();
    file.open(fileName_.c_str(),std::ios::in);
    while( std::getline(file, csvLine) ){
      std::istringstream csvStream(csvLine);
      while( std::getline(csvStream,val, ',') ){
        m_Dataij(temprows,tempcols) = std::atoi(val.c_str());
        tempcols++;
      }
      temprows++,tempcols = 0;
    }
  }
  else
  {
    MatrixReal tempdata(rows,cols);
    m_Dataij.resize(rows-1,cols-1);
    temprows = 0 , tempcols = 0;
    file.close();
    file.open(fileName_.c_str(),std::ios::in);
    while( std::getline(file, csvLine) ){
      std::istringstream csvStream(csvLine);
      while( std::getline(csvStream,val, ',') ){
        tempdata(temprows,tempcols) = std::atoi(val.c_str());
        tempcols++;
      }
      temprows++,tempcols = 0;
    }

    m_Dataij     = tempdata.sub(STK::Range(1,rows-1), STK::Range(1,cols-1));
    v_RowLabels_ = tempdata.col(STK::Range(1,rows-1), 0);
    v_ColLabels_ = tempdata.row(0, STK::Range(1,cols-1));
    m_Dataij.shift(0,0);
    v_RowLabels_.shift(0);
    v_ColLabels_.shift(0);
  }
  return true;
}

bool ContingencyData::readmui(std::string const& file_name)
{
  std::fstream file;
  file.open(file_name.c_str());
  if (!file.is_open()) {
#ifdef COVERBOSE
    std::cout<<"Input file cannot be open for reading.";
#endif
    return false;
  }
  std::string val;
  int rows = 0;
  while( std::getline(file, val) ){
    rows++;
  }


  v_Mui.resize(rows);
  rows = 0;
  file.close();
  file.open(file_name.c_str(),std::ios::in);
  while( std::getline(file, val) ){
    v_Mui[rows] = std::atoi(val.c_str());
    rows++;
  }
  return true;
}

bool ContingencyData::readnuj(std::string const& file_name)
{
  std::fstream file;
  file.open(file_name.c_str());
  if (!file.is_open()) {
#ifdef COVERBOSE
    std::cout<<"Input file cannot be open for reading.";
#endif
    return false;
  }
  std::string val;
  int rows = 0;
  while( std::getline(file, val) ){
    rows++;
  }

  v_Nuj.resize(rows);
  rows = 0;
  file.close();
  file.open(file_name.c_str(),std::ios::in);
  while( std::getline(file, val) ){
    v_Nuj[rows] = std::atoi(val.c_str());
    rows++;
  }
  return true;
}
