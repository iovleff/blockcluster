/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2013  Parmeet Singh Bhatia

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
 */

/** @file ContingencyData.h
 *  @brief Declares ContingencyData class that read Contingency data from  text files.
 **/

#ifndef CONTINGENCYDATA_H_
#define CONTINGENCYDATA_H_

/** @brief This class provides functions for contingency data handling.
 *
 */
#include "IData.h"
#include "../typedefs/typedef.h"

class ContingencyData: public IData
{
  public:
    ContingencyData(std::string const& file_name);
    virtual ~ContingencyData();

    virtual bool read(bool);

    bool readmui(std::string const& file_name);
    bool readnuj(std::string const& file_name);

    MatrixReal const& dataij() const { return m_Dataij;}
    VectorReal const& mui() const {return v_Mui;}
    VectorReal const& nuj() const {return v_Nuj;}

  protected:
    MatrixReal m_Dataij;
    VectorReal v_Mui,v_Nuj;

};


#endif /* CONTINGENCYDATA_H_ */
