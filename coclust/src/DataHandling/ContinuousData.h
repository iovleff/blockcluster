/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2013  Parmeet Singh Bhatia

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
 */

/** @file ContinuousData.h
 *  @brief Declares ContinuousData class that read Continuous data from  text files.
 **/
#ifndef CONTINUOUSDATA_H_
#define CONTINUOUSDATA_H_

/** @brief This class provides functions for Continuous data handling.
 *
 */
#include "IData.h"
#include "../typedefs/typedef.h"

class ContinuousData: public IData
{
  public:
    ContinuousData(std::string const& file_name);
    virtual bool read(bool);
    MatrixReal const& dataij() const { return m_Dataij;}
    virtual ~ContinuousData(){};
  protected:
    MatrixReal m_Dataij;

};


#endif /* CONTINUOUSDATA_H_ */
