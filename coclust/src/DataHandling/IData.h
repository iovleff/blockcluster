/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2012  Serge Iovleff

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place,
 Suite 330,
 Boston, MA 02111-1307
 USA

 Contact : Serge.Iovleff@stkpp.org
 */

/** @file IData.h
 *  @brief Declares abstract class IData for data handling.
 **/

#ifndef IDATA_H_
#define IDATA_H_

#include <string>
#include <vector>
#include "../typedefs/typedef.h"
/** @brief This is an interface class for handling data.
 *
 */
class IData
{
  public:
    IData(){};
    IData(std::string const& file_name) : fileName_(file_name) {};

    inline virtual ~IData() { }

    /** read the data needed by the model. This function internally call IData::read
     * @param file_name the name of the file with the data
     * @param semisupervised True if Row and Column labels are provided with the data.
     * @return @c true if the data were read successfully, @c false otherwise
     **/
    bool read(std::string const& file_name,bool semisupervised);
    /**Interface function to read the input data.*/
    virtual bool read(bool) = 0;
    /** Get Row labels for semi-supervised coclustering*/
    inline const VectorInteger& GetRowLabels() const {return v_RowLabels_;}
    /** Get Column labels for semi-supervised coclustering*/
    inline const VectorInteger& GetColLabels() const {return v_ColLabels_;}

  protected:
    std::string fileName_;
    VectorInteger v_RowLabels_ , v_ColLabels_;
};

inline bool IData::read( std::string const& file_name,bool semisupervised)
{
  fileName_ = file_name;
  return read(semisupervised);
}

#endif /* IDATA_H_ */
