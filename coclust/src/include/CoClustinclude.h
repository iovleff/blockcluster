
/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2013  Parmeet Singh Bhatia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : parmeet.bhatia@inria.fr , bhatia.parmeet@gmail.com
*/


/** @file CoClustinclude.h
 *  @brief Include file.
 **/

/** \mainpage CoClust Project
 *
 * \section intro_sec Introduction
 *
 * Simultaneous clustering of rows and columns, usually designated by
    biclustering, co-clustering or block clustering, is an important technique
    in two way data analysis. It consists of estimating a mixture model which
    takes into account the block clustering problem on both the individual and
    variables sets. This C++ software library is developed to
    allow co-clustering of Binary, contingency and continuous data-sets. The library is also
    available in form of R package. This package may be
    useful for various applications in fields of Data mining, Information
    retrieval, Biology, computer vision and many more.
 */
#ifndef COCLUSTINCLUDE_H_
#define COCLUSTINCLUDE_H_


//Library
#include "../CoClustFacade/CoCluster.h"

//type definitions
#include "../typedefs/typedef.h"

//Data
#include "../DataHandling/IData.h"
#include "../DataHandling/BinaryData.h"
#include "../DataHandling/ContinuousData.h"
#include "../DataHandling/ContingencyData.h"

//Input options
#include "../InputParameters/InputParameters.h"

//Strategy
#include "../Strategy/IStrategy.h"
#include "../Strategy/XStrategyAlgo.h"
#include "../Strategy/XStrategyforSEMAlgo.h"

//Algorithms
#include "../Algorithms/IAlgo.h"
#include "../Algorithms/EMAlgo.h"
#include "../Algorithms/CEMAlgo.h"
#include "../Algorithms/SEMAlgo.h"
#include "../Algorithms/GibbsAlgo.h"

//Models
#include "../Models/ICoClustModel.h"
#include "../Models/BinaryLBModel.h"
#include "../Models/BinaryLBModelequalepsilon.h"
#include "../Models/ContinuousLBModel.h"
#include "../Models/ContinuousLBModelequalsigma.h"
#include "../Models/ContingencyLBModel.h"
#include "../Models/ContingencyLBModel_mu_i_nu_j.h"
#include "../Models/CategoricalLBModel.h"

//Initialization
#include "../Initialization/IInit.h"
#include "../Initialization/CEMInit.h"
#include "../Initialization/RandomInit.h"

//STK Library
#include <STKpp.h>

#endif /* COCLUSTINCLUDE_H_ */
