/*--------------------------------------------------------------------*/
/*     Copyright (C) 2011-2012  Parmeet Singh Bhatia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : bhatia.parmeet@gmail.com , parmeet.bhatia@inria.fr
*/

/*
 * Project:  coclust::
 * created on: Nov 3, 2012
 * Purpose:  .
 * Author:   modal-laptop, parmeet.bhatia@inria.fr
 *
 **/

/** @file IntegrationTests.cpp
 *  @brief This file contains many integration tests.
 *
 **/

#include <memory>
#include <string>
#include <typeinfo>
#include "gtest/gtest.h"
#include "../src/include/CoClustinclude.h"

class ModelTest: public testing::Test
{

  protected:
  StrategyParameters Sparam_;
  ModelParameters Mparam_;
  //declaring pointer as shared to avoid overhead of 'delete' in various tests
  std::shared_ptr<CoCluster> p_CoCluster_;
  std::shared_ptr<IStrategy> p_Strategy_;
  std::shared_ptr<ICoClustModel> p_Model_;
  std::shared_ptr<IInit> p_Init_;
  std::shared_ptr<IAlgo> p_Algo_;

  virtual void SetUp()
  {
    //set up cocluster
    p_CoCluster_.reset(new CoCluster());
    //Set Strategy Parameters
    Sparam_.nbtry_ = 2;
    Sparam_.nbxem_ = 5;
    Sparam_.nbiter_xem_ = 100;
    Sparam_.nbiter_XEM_ = 500;
    Sparam_.Stop_Criteria = &ICoClustModel::ParameterStopCriteria;
    //Set Model Parameters
    Mparam_.eps_xem_ = .00001;
    Mparam_.eps_XEM_ = .00000001;
    Mparam_.nbinititerations_ = 10;
    Mparam_.initepsilon_ = .01;
    Mparam_.epsilon_int_ = .01;
    Mparam_.nbiterations_int_ = 5;
    Mparam_.nbrowclust_ = 2;
    Mparam_.nbcolclust_ = 3;
    //set Mparam_.fixedproportions_ in tests
  }

  //setup for cocluster
  void SetCoCluster(  std::shared_ptr<IStrategy> p_Strategy_, std::shared_ptr<ICoClustModel> p_Model_,
  std::shared_ptr<IInit> p_Init_, std::shared_ptr<IAlgo> p_Algo_)
  {
    p_CoCluster_->SetAlgo(p_Algo_.get());
    p_CoCluster_->SetInit(p_Init_.get());
    p_CoCluster_->SetModel(p_Model_.get());
    p_CoCluster_->SetStrategy(p_Strategy_.get());
  }
};


TEST_F(ModelTest,BinaryLBModel)
{
  std::shared_ptr<BinaryLBModel> p_BinaryLBModel_;
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/BinaryData/testbinarypikrholepskl.dat";
  std::shared_ptr<BinaryData> p_Data_(new BinaryData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";

  p_Init_.reset(new CEMInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new BinaryLBModel(p_Data_->dataij(),Mparam_));

  //EM Algorithm
  p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();
  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModel*>(p_Model_->Clone()));
  EXPECT_TRUE(((p_BinaryLBModel_->Getdispersion()).array()<.5).any()&&
              ((p_BinaryLBModel_->Getdispersion()).array()>=0).any())
  <<"EM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"EM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";

  //CEM Algorithm
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModel*>(p_Model_->Clone()));
  EXPECT_TRUE(((p_BinaryLBModel_->Getdispersion()).array()<.5).any()&&
              ((p_BinaryLBModel_->Getdispersion()).array()>=0).any())
  <<"CEM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"CEM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";


  //SEM Algorithm
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModel*>(p_Model_->Clone()));
  EXPECT_TRUE(((p_BinaryLBModel_->Getdispersion()).array()<.5).any()&&
              ((p_BinaryLBModel_->Getdispersion()).array()>=0).any())
  <<"SEM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"SEM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";


}

TEST_F(ModelTest,BinaryLBModelequalepsilon)
{
  std::shared_ptr<BinaryLBModelequalepsilon> p_BinaryLBModel_;
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/BinaryData/testbinarypikrholeps.dat";
  std::shared_ptr<BinaryData> p_Data_(new BinaryData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";

  p_Init_.reset(new CEMInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new BinaryLBModelequalepsilon(p_Data_->dataij(),Mparam_));

  //EM Algo
  p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();

  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModelequalepsilon*>(p_Model_->Clone()));
  EXPECT_TRUE((p_BinaryLBModel_->Getdispersion()<.5)&&
              (p_BinaryLBModel_->Getdispersion()>=0))
  <<"EM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"EM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";


  //CEM Algo
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModelequalepsilon*>(p_Model_->Clone()));
  EXPECT_TRUE((p_BinaryLBModel_->Getdispersion()<.5)&&
              (p_BinaryLBModel_->Getdispersion()>=0))
  <<"CEM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"CEM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";

  //SEM Algo
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  //checking estimated parameter values (pi,rho,thetha)
  p_BinaryLBModel_.reset(dynamic_cast<BinaryLBModelequalepsilon*>(p_Model_->Clone()));
  EXPECT_TRUE((p_BinaryLBModel_->Getdispersion()<.5)&&
              (p_BinaryLBModel_->Getdispersion()>=0))
  <<"SEM Algo: Dispersion not in range\n";
  for (int i = 0; i < Mparam_.nbrowclust_; ++i) {
    for (int j = 0; j < Mparam_.nbcolclust_; ++j) {
      EXPECT_TRUE(p_BinaryLBModel_->Getmean()(i,j)==0||p_BinaryLBModel_->Getmean()(i,j)==1)
          <<"SEM Algo: Center for row cluster "<< i<<" and column cluster "<<j<<"is not 0 or 1\n";
    }
  }
  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";

}

TEST_F(ModelTest,ContinuousLBModel)
{
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/GaussianData/testgaussianpikrholsigma2kl.dat";
  std::shared_ptr<ContinuousData> p_Data_(new ContinuousData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";

  p_Init_.reset(new CEMInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new ContinuousLBModel(p_Data_->dataij(),Mparam_));

  //EM Algo
 p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";

  //CEM Algo
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";
  //SEM Algo
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";
}


TEST_F(ModelTest,ContinuousLBModelequalsigma)
{
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/GaussianData/testgaussianpikrholsigma2.dat";
  std::shared_ptr<ContinuousData> p_Data_(new ContinuousData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";

  p_Init_.reset(new CEMInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new ContinuousLBModelequalsigma(p_Data_->dataij(),Mparam_));

  //EM Algo
  p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";

  //CEM Algo
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";

  //SEM Algo
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";
}

TEST_F(ModelTest,ContingencyLBModel)
{
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/ContingencyData/testpoissonpirhounknown.dat";
  std::shared_ptr<ContingencyData> p_Data_(new ContingencyData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";

  p_Init_.reset(new CEMInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new ContingencyLBModel(p_Data_->dataij(),Mparam_));

  //EM Algo
  p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";

  //CEM Algo
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";

  //SEM Algo
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";
}

TEST_F(ModelTest,ContingencyLBModel_mu_i_nu_j)
{
  std::string filename = "/home/modal-laptop/workspace/coclust/Data/ContingencyData/testpoissonpirhoknown.dat";
  std::string mui = "/home/modal-laptop/workspace/coclust/Data/ContingencyData/testmui.dat";
  std::string nuj = "/home/modal-laptop/workspace/coclust/Data/ContingencyData/testnuj.dat";

  std::shared_ptr<ContingencyData> p_Data_(new ContingencyData(filename));
  ASSERT_TRUE(p_Data_->read(false))<<"Cannot read data from file: "<<filename<<"\n";
  ASSERT_TRUE(p_Data_->readmui(mui))<<"Cannot read data from file: "<<filename<<"\n";
  ASSERT_TRUE(p_Data_->readnuj(nuj))<<"Cannot read data from file: "<<filename<<"\n";
  p_Init_.reset(new RandomInit());
  p_Strategy_.reset(new XStrategyAlgo(Sparam_));
  Mparam_.fixedproportions_ = false;
  Mparam_.nbrowdata_ = (p_Data_->dataij()).rows();
  Mparam_.nbcoldata_ = (p_Data_->dataij()).cols();
  p_Model_.reset(new ContingencyLBModel_mu_i_nu_j(p_Data_->dataij(),p_Data_->mui(),p_Data_->nuj(),Mparam_));

  //EM Algo
  p_Algo_.reset(new EMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"EM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"EM Algo: Sum of column proportions not equal to one\n";

  //CEM Algo
  p_Algo_.reset(new CEMAlgo());
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"CEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"CEM Algo: Sum of column proportions not equal to one\n";

  //SEM Algo
  p_Algo_.reset(new SEMAlgo());
  p_Strategy_.reset(new XStrategyforSEMAlgo(Sparam_));
  SetCoCluster(p_Strategy_,p_Model_,p_Init_,p_Algo_);
  ASSERT_TRUE(p_CoCluster_->run())<<"SEM Algo: "<<p_Model_->GetErrormsg();

  EXPECT_TRUE(p_Model_->GetRowProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of row proportions not equal to one\n";
  EXPECT_TRUE(p_Model_->GetColProportions().array().sum()-1.0<.00001)
  <<"SEM Algo: Sum of column proportions not equal to one\n";
}

